#!/usr/bin/env bash
set -euo pipefail

dir="$(dirname "$0")"

image="latex-build"
cd "$dir"
exec docker build -t "$image" .
