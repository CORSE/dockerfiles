#!/usr/bin/env bash
set -euo pipefail

dir="$(dirname "$0")"
version="${DOCKER_VERSION-latest}"
host="${DOCKER_HOST-registry.gitlab.inria.fr}"
repo="${DOCKER_REPO-corse/dockerfiles/latex-build}"
image="${DOCKER_IMAGE-$host/$repo:$version}"
umask=$(umask)
docker pull "$image" || true # Tentative pull for being up to date
exec docker run --rm -it -u $(id -u):$(id -g) -v $PWD:$PWD -w $PWD "$image" bash -c "umask $umask; $*"
