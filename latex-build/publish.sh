#!/usr/bin/env bash
set -euo pipefail

dir="$(dirname "$0")"

version="$(cat "$dir"/VERSION)"
host="registry.gitlab.inria.fr"
repo="corse/dockerfiles/latex-build"
image="$host/$repo:$version"
image_latest="$host/$repo:latest"
local_image="latex-build"

cd "$dir"
echo "Checking not published: $image"
! docker manifest inspect "$image" >/dev/null 2>&1 || \
    { echo "ERROR: version already exists on registry: $image" >&2; exit 1; }
echo "Tagging: $local_image -> $image"
docker tag "$local_image" "$image"
echo "Tagging: $local_image -> $image_latest"
docker tag "$local_image" "$image_latest"
echo "Pushing: $image"
docker push "$image"
echo "Pushing: $image_latest"
docker push "$image_latest"
