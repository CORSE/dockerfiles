Dockerfile for Latex build
==========================

Build and publish to the gitlab registry with:

    # Update VERSION file first
    ./build.sh
    ./publish.sh

WARNING: always increment VERSION file before executing `./build.sh`.

Then for instance one may use the generated image to build a typical
Latex publication, for instance with the provided `in-docker.sh` script:

    cd ~/paper-build
    ~/dockerfiles/latex-build/in-docker.sh make clean
    ~/dockerfiles/latex-build/in-docker.sh make all

Generally one may want to copy the `in-docker.sh` script in its own repo
and optionally force the `version` in the script to a specific version:

    cd ~/paper-build
    cp ~/dockerfiles/latex-build/in-docker.sh .
    ./in-docker.sh make all
